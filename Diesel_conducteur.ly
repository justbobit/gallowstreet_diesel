\version "2.18.2"

\include "TromUn.ily"
\include "TromDeux.ily"
\include "TromTrois.ily"
\include "TrumpUn.ily"
\include "TrumpDeux.ily"
\include "TrumpTrois.ily"
\include "AltSaxUn.ily"
\include "TenorOne.ily"
\include "saxBar.ily"
\include "Bass.ily"
\include "clar.ily"


#(set! paper-alist (cons '("my size" . (cons (* 29.7 cm) (* 21 cm)))
 paper-alist))

\paper {
  #(set-paper-size "my size")
  ragged-bottom = ##t
  % For 3 page layout:
  #(layout-set-staff-size 13)
  top-margin    =6
  bottom-margin =6
  left-margin   =6
  right-margin  =6
  
  %annotate-spacing = ##t
  %{ %}
  between-system-padding = 0
  ragged-last-bottom = ##f
}


\header
{
  title    = "Gallowstreet - Diesel"
  composer = "Arrgt. Bobby V1"
  tagline = ##f
}


\layout {
  \context {
    \Score 
    \override StaffGrouper.staff-staff-spacing.padding = #0
    \override StaffGrouper.staff-staff-spacing.basic-distance = #1
  }
}

<<
  \new StaffGroup <<
    \new Staff { #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Clarinet"
          \set Staff.midiInstrument = #"clarinet" %"
          \set Score.markFormatter = #format-mark-box-alphabet
          \transpose a d' {\relative c''' {\key c \major \unfoldRepeats
           \ClarUn} }}
    \new Staff { #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Alto Sax"
          \set Staff.midiInstrument = #"alto sax" %"
          \set Score.markFormatter = #format-mark-box-alphabet
          \relative c''' {\key c \major \unfoldRepeats \AltSaxUn} }
    \new Staff { #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Tenor Sax"
          \set Staff.midiInstrument = #"tenor sax" %"
          \set Score.markFormatter = #format-mark-box-alphabet
          \relative c' {\key f \major \unfoldRepeats \TenorSaxUn} }
    \new Staff { #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Baritone Sax"
          \set Staff.midiInstrument = #"tenor sax" %"
          \set Score.markFormatter = #format-mark-box-alphabet
          \relative c'' {\key c \major \unfoldRepeats \BarSax} }
  >>

  \new StaffGroup <<
    \new Staff{  #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Trumpet 1"
        \set Score.markFormatter = #format-mark-box-alphabet
        \relative c'' {\clef "treble" \key f \major
      \unfoldRepeats \TrumpUn}}
    \new Staff{#(set-accidental-style 'modern)
          \set Staff.instrumentName = "Trumpet 2"
          \set Score.markFormatter = #format-mark-box-alphabet
          \relative c' {\clef "treble" \key f \major \large
         \unfoldRepeats \TrumpDeux}}
    \new Staff{ #(set-accidental-style 'modern)
      \set Staff.instrumentName = "Trumpet 3"
      \set Score.markFormatter = #format-mark-box-alphabet
      \relative c' {\clef "treble" \key f \major \large
    \unfoldRepeats  \TrumpTrois} }
  >>

  \new StaffGroup <<
  \new Staff { #(set-accidental-style 'modern)
    \set Staff.instrumentName = "Trombone 1"
    \set Score.markFormatter = #format-mark-box-alphabet
    \relative c' {\key ees \major \clef "bass" \large \unfoldRepeats \TromUn}}
  \new Staff {#(set-accidental-style 'modern)
    \set Staff.instrumentName = "Trombone 2"
    \set Score.markFormatter = #format-mark-box-alphabet
    \relative c' {\key ees \major \clef "bass" \large \unfoldRepeats\TromDeux}}
  \new Staff {#(set-accidental-style 'modern)
    \set Staff.instrumentName = "Trombone 3"
    \set Score.markFormatter = #format-mark-box-alphabet
    \relative c' {\key ees \major \clef "bass" \large \unfoldRepeats\TromTrois}}
  \new Staff{#(set-accidental-style 'modern)
    \set Staff.instrumentName = "Eupho"
    \set Score.markFormatter = #format-mark-box-alphabet
    \transpose c d {\relative c'' {\clef "treble" \key c \minor \large
  \unfoldRepeats \TromTrois}}}
  >>
  \new StaffGroup <<
  \new Staff{#(set-accidental-style 'modern)
    \set Staff.instrumentName = "Sousa"
    \set Score.markFormatter = #format-mark-box-alphabet
    \transpose c d {\relative c' {\clef "treble" \key c \minor \large
  \unfoldRepeats \Sousa}}}
  >>
>>