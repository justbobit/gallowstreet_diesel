\version "2.18.2"

ClarIntro = {
  a1(\p^\markup{Intro}\tempo 4=100 | f2 e4  \tuplet 3/2 {c b8} | a1)
  gis2( b4 \tuplet 3/2 {c d8} | c2. b4 | a2.) \tuplet 3/2 {f4( e8} | d1)
  d'2( e4 f | g1) | gis\<\fermata
}

ClarL = {
  a4 a8 r a-. gis-. a-. gis-. a-. bes4 bes8 |
  \repeat unfold 2 {a4 a8 r a-. gis-. a-. gis-. a-. bes4 bes8} |
  a4 a8 r a-. gis-. a-. gis-. a-. bes-. d-. bes-. |
}

ClarUn =
{
  \ClarIntro

  \mark \default \tempo 4 = 132 %A
  R1*8\!

  \mark \default %B
  \repeat volta 2 {r8 a,\mf r a r a r a16( bes) |
  r8 a r a r a r a16( bes)    |
  a8-. a r a r a r a16( bes)  |
  r8 a r a g4-- a--           |}


  \mark \default %C
  \tuplet 3/2 {a8\<\f \repeat unfold 8 {a}} a4 |
  \tuplet 3/2 {\repeat unfold 9 {a8}} a4 |
  \tuplet 3/2 {\repeat unfold 9 {c8}} c4\!\ff |
  e4-> c -> bes-> f'->

  \mark \default %D
  bes,--\f bes'2~ bes8 g  | a-. e~ e2.    |
  bes4  bes'2~ bes8 g     | a-. f16 d e2. |
  f4-- d2 e8-. c16 a~      | a1 |
  bes4-- d2 e8-. c16 a16~ | a1 |

  \mark \default %E
  bes4-- bes'2~ bes8 g  | a-. e~ e2.    |
  r4  bes'2~ bes8 g     | a-. f16 d e2. |
  f4-- d2 e8-. c16 a~      | a1 |
  bes4-- d2 e8-. bes16 c16~ | c1 |

  \mark \default %F
  R1*4

  \mark \default %G
  R1*8

  \mark \default %H
  R1*7
  e4->\ff c-> bes-> f'->

  \mark \default %I
  bes,--\f bes'2~ bes8 g  | a-. e~ e2.    |
  bes4  bes'2~ bes8 g     | a-. f16 d e2. |
  f4-- d2 e8-. c16 a~      | a1 |
  bes4-- d2 e8-. c16 a16~ | a1 |

  \mark \default %J
  bes4-- bes'2~ bes8 g  | a-. e~ e2.    |
  r4  bes'2~ bes8 g     | a-. f16 d e2. |
  f4-- d2 e8-. c16 a~      | a1 |
  bes4-- d2 e8-. bes16 c16~ | c1 |

  \mark \default %K
  \tuplet 3/2 {a8\mf \repeat unfold 8 {a}} a4-.    |
  \tuplet 3/2 {c8\< \repeat unfold 8 {c}} c4-.    |
  \tuplet 3/2 {e8 \repeat unfold 8 {e}} e4-.\!\ff |
  \tuplet 3/2 {a8 \repeat unfold 8 {a}} a4-.      |

  \mark \default  \time 12/8 \tempo 4. = 132 %L
  \repeat volta 2 {
    a,4\f a8 r a-. gis-. a-. gis-. a-. bes4 bes8 |
    \repeat unfold 2 {a4 a8 r a-. gis-. a-. gis-. a-. bes4 bes8} |
    a4 a8 r a-. gis-. a-. gis-. a-. bes-. d-. bes-. |
  }

  \mark \default %M
  \repeat volta 2 {\ClarL}

  \mark \default %N
  \repeat volta 2 {a4 a8 r a-. gis-. a-. gis-. a-. bes4 bes8 |
  \repeat unfold 2 {a4 a8 r a-. gis-. a-. gis-. a-. bes4 bes8} |}
  \alternative{
    {a4 a8 r a-. gis-. a-. gis-. a-. bes-. d-. bes-. |}
    {a4 a8 r a-. gis-. a-. gis-. a-. c4. |}
  }
  

  \mark \default %O
  a2.~ a4. gis4-. c8 | a2.~ a4. a4-. c8 |
  d4. e4-. c8 d4. c4-. gis8 | e2.~ e4. c'4.-- |

  a2.~ a4. gis4-. c8 | a2.~ a4. a4-. c8 |
  d4. e4-. c8 d4. e4-. g8 | e1. |

  \mark \default  \time 4/4 \tempo 4 = 132 %P
  % e1~ | e~ | e~ | e | e~ | e | d~ | d\fermata \bar"|."
  \transpose g d' { \relative c'{f1( | e | d | cis) | f( | g | f | g)}}\fermata  % taken from trumpUn_P
  % fin
  e4-> c -> bes-> f'->
  \bar"|."

}


