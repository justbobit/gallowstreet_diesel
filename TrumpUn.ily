\version "2.18.2"

TrumpUnO = {
	d,2.~ d4. cis4-. e8 |  d2.~ d4. d4-.f8 |
	g4. a4-. f8 g4. f4-. cis8 | a2.~ a4. f'4.--

	d2.~ d4. cis4-. e8 |  d2.~ d4. d4-.f8 |
	g4. a4-. f8 g4. a4-. c8 | a1. |
}

TrumpUn ={
	a1(\p^\markup{Intro}\tempo 4=100 | bes2 g | a1) | 
	g2( bes | d2.) bes4( | a1 | bes) | g2( a4 bes4 | c1) | cis1\fermata\<

	\mark \default \tempo 4 = 132 %A
	R1*8\!
	\mark \default  %B
	R1*8
	
	\mark \default  %C
	\tuplet 3/2 {r8\f\< d d \repeat unfold 2 {r d d} } d4|
	\repeat unfold 3 {\tuplet 3/2 {r8 e e}} e4 |
	\repeat unfold 3 {\tuplet 3/2 {r8 f f}} f4 |
	g4-> ees-> d-> a'-> |
	
	\mark \default  %D
	ees,--\f ees'2~ ees8 c | d-. a~ a2. | ees4-- ees'2~ ees8 c | d-. bes16 g a2.
	bes4-- g2 a8-. f16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default  %E
	g4-- g'2~ g8 ees | f-. d~ d2. | r4 g2~-> g8 ees | f-. ees16 c d2. |
	ees4-- c2 d8-. bes16 a~ | a1 | bes4-- d2 ees8-. bes16 a~ | a1 |

	\mark \default  %F
	R1*4 | 
	\mark \default  %G
	R1*8 |

	\mark \default %H

	R1*2 | d16\>\f d r16 d d r d d r d d r d d r d | d r d d\!\p r4 r2 |
	R1*2 | d16\>\f d r16 d d r d d r d d r d d r d\!\p | g4\ff-> ees-> d-> a'-> |

	\mark \default %I
	ees,4--\f ees'2~ ees8 c | d-. a~ a2. | ees4-- ees'2~ ees8 c | d-. bes16 g a2. |
	bes4-- g2 a8-. f16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default %J
	g4-- g'2~ g8 ees | f-. d~ d2. | r4 g2~-> g8 ees | f-. ees16 c d2. |
	ees4-- c2 d8-. bes16 a~ | a1 | bes4-- d2 ees8-. bes16 a~ | a1 |

	\mark \default %K
	\repeat unfold 3 {\tuplet 3/2 {d8 d d}} d4-. |
	\tuplet 3/2 {e8\< \repeat unfold 8 {e} } e4-. |
	\tuplet 3/2 {f8 \repeat unfold 8 {f} } f4-.\! |
	\tuplet 3/2 {a8\ff \repeat unfold 8 {a} } a4-. |

	\mark \default \time 12/8 \tempo 4. = 132 %L
	R1.*8 |
	\mark \default %M
	R1.*8 |
	\mark \default %N
	\repeat percent 4 {d,8-.\f c-. bes-. c-. bes-. a-. bes-. a-. g-. a-. r r  |}

	\repeat percent 4 {g'-. f-. ees-. f-. ees-. d-. ees-. d-. c-. d-. r r | }

	\mark \default %O
	\TrumpUnO

	\mark \default \time 4/4 \tempo 4 = 132 %P
	\transpose c d { \relative c''{\TUNnotesP}}

	g'4-> ees-> d-> a'->
	\bar"|."
	% f'1( | e | d | cis) | f( | g | f | g)\fermata \bar"|."
}