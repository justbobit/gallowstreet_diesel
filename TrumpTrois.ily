% probleme dans l'attaque avant le theme. Vérifier la partition des gallowstreet qui m'avait été envoyée


\version "2.18.2"
\include "TrumpUn.ily"


TrumpTrois ={
	d1(\p^\markup{Intro}\tempo 4=100 | f2 e2 | f1) | 
	e2( g | a2.) g4( | f1 | g1) | f2 e4 f | e1 | e\<\fermata |

	\mark \default %A
	R1*8\!

	\mark \default %B
	R1*8

	\mark \default %C
	\tuplet 3/2 {r8\f a a r a a r a a} a4 |
	\tuplet 3/2 {\repeat unfold 3 {r8 a a} } a4 |
	\tuplet 3/2 {\repeat unfold 3 {r8 a a} } a4\! |
	bes->\ff bes a ees'->

	\mark \default %D
	ees,4--\f bes'2~ bes8 g | a-. f~ f2. | ees4 bes'2~ bes8 g | a-. f16 ees f2. |
	g4-- ees2 g8-. ees16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default %E
	ees4-- bes'2~ bes8 g | a-. f~ f2. | r4 bes'2~ bes8 g | a-. f16 ees f2. |
	g4-- ees2 g8-. ees16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default %F
	R1*4 

	\mark \default %G
	R1*8

	\mark \default %H
	R1*2 | f16\>\f f r16 f f r f f r f f r f f r f | f r f f\!\p r4 r2 |
	R1*2 | f16\>\f f r16 f f r f f r f f r f f r f\!\p | bes4-> bes a-> ees-> |

	\mark \default %I
	ees,4--\f bes'2~ bes8 g | a-. f~ f2. | ees4 bes'2~ bes8 g | a-. f16 ees f2. |
	g4-- ees2 g8-. ees16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default %J
	ees4-- bes'2~ bes8 g | a-. f~ f2. | r4 bes2~ bes8 g | a-. f16 ees f2. |
	g4-- ees2 g8-. ees16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default %K
	\tuplet 3/2 {f8\f \repeat unfold 8 {f} } f4-. |
	\tuplet 3/2 {a8\<\repeat unfold 8 {a8} } a4-. |
	\tuplet 3/2 {a8\repeat unfold 8 {a8} } a4-.\! |
	\tuplet 3/2 {a8\ff \repeat unfold 8 {a8} } a4-. |

	\mark \default \time 12/8 \tempo 4. = 132 %L
	R1.*8
	\mark \default %M
	R1.*8

	\mark \default %N
	\repeat percent 8 {a8-.\f g-. f-. g-. f-. ees-. f-. ees-. d-. ees-. r r |}

	\mark \default %O
	\relative c'' {\TrumpUnO}

	\mark \default \time 4/4 \tempo 4 = 132 %P
	a1( | e | d | cis) | f( g | bes | bes)\fermata 

	bes4-> bes a ees'->
	\bar"|."

}