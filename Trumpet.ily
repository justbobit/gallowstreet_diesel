\version "2.18.2"

Trumpet = {
	<c ees g>1\p^\markup{Intro}   | <c ees aes>2 <bes d f> | 
	<c ees g>2. r4                | <d g b>2\< <d f aes> |
	<ees g c>2.\!\mp\> <d f aes>4 | <c ees g>2.\!\p r4 |
	<c f aes>2. r4                | <c f>2  <ees g>4 <f aes> | <d f bes>1\< |
	<d g b>1\!\mp\fermata
	\mark "A" \tempo 4 = 132
	R1* 8 | R1*8
	
}

#(set! paper-alist (cons '("my size" . (cons (* 21 cm) (* 29.7 cm)))
 paper-alist))



\paper {
  #(set-paper-size "my size")
  ragged-bottom = ##t
  % For 3 page layout:
  #(layout-set-staff-size 13)
  top-margin    =6
  bottom-margin =6
  left-margin   =6
  right-margin  =6
  
  %annotate-spacing = ##t
  %{ %}
  between-system-padding = 0
  ragged-last-bottom = ##f
}

\book 
{
    \header
    {
      title    = "Gallowstreet - Diesel"
      composer = "Arrgt. Bobby V1"
      tagline = ##f
    }
  \score
  {
    \context StaffGroup {
    	<<
				\new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Trompette 1"
          \transpose c d {\relative c' {\clef "treble" \key c \minor \large
                  \Trumpet}}
        }
      >>

    }
    \layout {
    \compressFullBarRests \override MultiMeasureRest.expand-limit = #1
    \set Score.markFormatter = #format-mark-box-alphabet }
  }
}