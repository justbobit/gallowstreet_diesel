\version "2.18.2"


TUNnotesM = {
	 \repeat percent 2 { r2. r4. aes8 g b | r2. aes8 g b aes g b |}
}

TUNnotesO = {
	c2.~ c4. b4-. ees8          | c2.~ c4. c4 ees8 | 
f4. g4-. ees8 f4. ees4-. b8 |  g2.~ g4. ees'4.-- | 

c2.~ c4. b4-. ees8          | c2.~ c4. c4 ees8 | 
f4. g4-. ees8 f4. g4-. bes8 |  g1. | 

}

TUNnotesP = {
	g2. \tuplet 3/2 {bes4-. bes8} | g2. \tuplet 3/2 {g4-. bes8} | 
c4-- \tuplet 3/2 {bes4 g8} f4-- \tuplet 3/2 {ees4 f8} | g1

g2. \tuplet 3/2 {bes4-. bes8} | g2. \tuplet 3/2 {g4-. bes8} | 
c4-- \tuplet 3/2 {bes4 g8} f4-- \tuplet 3/2 {ees4 d8} | des1\fermata  |
}

TromUn =
{
%Intro
r4^\markup{Intro}\tempo 4=100  ees\pp( d) r | 
\repeat percent 6 {r4  ees( d) r |}

r4 ees( d c | d1) | d\fermata\< |
\mark \default \tempo 4 = 132 %A
R1*8\!
\mark \default  %B
R1*5 | r2 r8 c8-. des bes | R1*2 |

\mark \default %C
\tuplet 3/2 {c8\<\f c c c c c c c c} c4        |
\tuplet 3/2 {aes8 \repeat unfold 8 {aes}} aes4 |
\tuplet 3/2 {g8 g g g g g g g g} g4\!          |

c->\ff aes-> g-> des'->

\mark \default %D
des,4\f des'2~ des8 bes | c-. g~ g2.
des4 des'2~ des8 bes | c-. aes16 f g2.
aes4-- f2 g8-. ees16 c~ | c1 |
des4-- f2 g8-. ees16 c~  | c1 |

\mark \default %E
des4 des'2~ des8 bes | c-. g~ g2.
r4 des'2~-> des8 bes | c-. aes16 f g2.
aes4-- f2 g8-. ees16 c~ | c1 |
des4-- f2 g8-. ees16 c~  | c1 |

\mark \default %F
R1*4

\mark \default %G
ees'1~( | ees2 f4 d  | ees1~ | ees)|
ees1~( | ees2  d4 b | c1~   | c) |

\mark \default %H
ees1~ | ees2 f4 g | ees1~ | ees |
ees1~  | ees2  d4 b | c1   | c4->\ff aes-> g-> des'->

\mark \default %I
des,4\f des'2~ des8 bes | c-. g~ g2.
des4 des'2~ des8 bes | c-. aes16 f g2.
aes4-- f2 g8-. ees16 c~ | c1 |
des4-- f2 g8-. ees16 c~  | c1 |

\mark \default %J
des4 des'2~ des8 bes | c-. g~ g2.
r4 des'2~-> des8 bes | c-. aes16 f g2.
aes4-- f2 g8-. ees16 c~ | c1 |
des4-- f2 g8-. ees16 c~  | c1 |

\mark \default %K
\tuplet 3/2 {c'8\f c c} \repeat unfold 2 {\tuplet 3/2 {c8 c c}} c4-.
\tuplet 3/2 {aes8\< aes aes} \repeat unfold 2 {\tuplet 3/2 {aes8 aes aes}} aes4-.
\tuplet 3/2 {ees8 ees ees} \repeat unfold 2 {\tuplet 3/2 {ees8 ees ees}} ees4-.
\tuplet 3/2 {c8\!\ff c c} \repeat unfold 2 {\tuplet 3/2 {c8 c c}} c4

\mark \default \time 12/8 \tempo 4. = 132 %L
R1.*8
\mark \default %M
\repeat volta 2 {\TUNnotesM}

\mark \default %N
\repeat percent 3 { r2. r4. aes8 g b | r2. aes8 g b aes g b |}
 r2. r4. aes8 g b |  r2. r4. ees'4. |

\mark \default%O
\TUNnotesO

\mark \default \time 4/4 \tempo 4 = 132 %P
\TUNnotesP

c4->\ff aes-> g-> des'->

\bar"|."
}