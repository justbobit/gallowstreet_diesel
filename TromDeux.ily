\include "TromUn.ily"



TromDeux = {
%Intro
r4^\markup{Intro}\tempo 4=100  c\pp( b) r | 
\repeat percent 6 {r  c( b) r | }
r4 c( b aes | bes1) | b\fermata\< |

\mark \default %A
R1*8\! 
\mark \default  %B
\repeat volta 2 {r8  g\mf r g r g r g16\glissando aes | 
r8  g    r g r g r g16\glissando aes |
g8-. g    r g r g r g16\glissando aes |
r8  g  r g bes4-- c --}

\mark \default %C
\tuplet 3/2 {g8\< g g g g g g g g} g4
\tuplet 3/2 {g8 g g g g g g g g } g4
\tuplet 3/2 {ees8 ees ees ees ees ees ees ees ees} ees4
g4->\ff ees-> des-> aes'->

\mark \default %D
des,4\f des'2~ des8 bes | c-. g~ g2.
des4 des'2~ des8 bes | c-. aes16 f g2.
aes4-- f2 g8-. ees16 c~ | c1 |
des4-- f2 g8-. ees16 c~  | c1 |

\mark \default %E
des4 des'2~ des8 bes | c-. g~ g2.
r4 des'2~-> des8 bes | c-. aes16 f g2.
aes4-- f2 g8-. ees16 c~ | c1 |
des4-- f2 g8-. ees16 c~  | c1 |

\mark \default %F
R1*4 |

\mark \default %G
	c'1~(\mf | c2 d4 b | c1~ | c) |
	c1~(     | c2 b4 aes | g1~ | g) 
\mark \default %H
	c1~( | c2 d4 ees | c1~ | c) |
	c1~( | c2 b4 aes | g1) | g4->\ff ees-> des-> aes'->

\mark \default %I
des,4\f des'2~ des8 bes | c-. g~ g2.
des4 des'2~ des8 bes | c-. aes16 f g2.
aes4-- f2 g8-. ees16 c~ | c1 |
des4-- f2 g8-. ees16 c~  | c1 |

\mark \default %J
des4 des'2~ des8 bes | c-. g~ g2.
r4 des'2~-> des8 bes | c-. aes16 f g2.
aes4-- f2 g8-. ees16 c~ | c1 |
des4-- f2 g8-. ees16 c~  | c1 |

\mark \default %K
\tuplet 3/2 {g'8\< g g g g g g g g} g4-.                                 |
\tuplet 3/2 {g8 g g g g g g g g} g4-.                                    |
\tuplet 3/2 {c,8 c c c c c c c c} c4-.|
\tuplet 3/2 {g8\!\ff g g } \repeat unfold 2 {\tuplet 3/2 {g8 g g }} g4-. |

\mark \default  \time 12/8 \tempo 4. = 132 %L
R1.*8

\mark \default % M
\repeat volta 2 {\TUNnotesM}

\mark \default % N
\repeat percent 3 { r2. r4. aes8 g b | r2. aes8 g b aes g b |}
 r2. r4. aes8 g b |  r2. r4. ees'4. |

\mark \default 
\TUNnotesO

\mark \default  \time 4/4 \tempo 4 = 132
\TUNnotesP

g,4->\ff ees-> des-> aes'-> 
\bar"|."

}