# DOCNAME=main3

all: Diesel_parties_separees Diesel_conducteur Gallowstreet_fullmidi

.PHONY: clean

Diesel_parties_separees:
	lilypond Diesel_parties_separees.ly

Diesel_conducteur:
	lilypond Diesel_conducteur.ly

Gallowstreet_fullmidi:
	lilypond Gallowstreet_fullmidi.ly
	fluidsynth -F fichier_audio.wav /usr/share/sounds/sf2/FluidR3_GM.sf2 Gallowstreet_fullmidi.midi

view: Diesel_parties_separees
	evince $(DOCNAME).pdf &

clean:
	rm *.blg *.bbl *.aux *.log