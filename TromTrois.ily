\version "2.18.2"

\include "TromUn.ily"

TTroiP = {
	c2. \tuplet 3/2 {ees4-.  ees8}                         |
	c2. \tuplet 3/2 {c4-. ees8}                            |
	f4  \tuplet 3/2 {g4-. ees8 } f4-- \tuplet 3/2 {ees4-. c8 } |
	g1                                                     |

	c2. \tuplet 3/2 {ees4-.  ees8}                         |
	c2. \tuplet 3/2 {c4-. ees8}                            |
	f4  \tuplet 3/2 {g4-. ees8 } f4-- \tuplet 3/2 {ees4-. d8 } |
	a1\fermata

}


TromTrois = {
	r4^\markup{Intro}\tempo 4=100 aes(\pp g) r |
	\repeat percent  6 {r4 aes( g) r |}
	r aes( g ees | f1) | f1\<

\mark \default \tempo 4 = 132 %A 
R1*8\!
\mark \default %B
R1*5 | r2 r8 g-. aes-. f-.  | R1*2

\mark \default %C
\tuplet 3/2 {g,8\<\f g g} \repeat unfold 2 {\tuplet 3/2 {g g g}} g4 | 
\repeat unfold 3 {\tuplet 3/2 {g8 g g}} g4 |
\repeat unfold 3 {\tuplet 3/2 {g8 g g}} g4 |
c4->\!\ff aes-> g-> des'->

\mark \default %D
f4--\f aes2~ aes8 f     | g-. ees~ ees2.
f4-- aes2~ aes8 f       | g-. ees16 f g2.
f4-- des2 ees8-. d16 c~ | c1
des4-- f2 ees8-. d16 c~ | c1

\mark \default %E
f4--\f aes2~ aes8 f     | g-. ees~ ees2.
r4 aes2~-> aes8 f       | g-. ees16 f g2.
f4-- des2 ees8-. d16 c~ | c1
des4-- f2 ees8-. d16 c~ | c1

\mark \default %F
R1*4


\mark \default %G
g'1~( | g2 aes4 f | g1~ | g) | g1~( | g2 f4 ees | ees1~ | ees)

\mark \default %H
g1~( | g2 aes4 bes | g1~ | g) | g1~( | g2 f4 ees | ees1) | c4->\ff aes g des'->

\mark \default %I
f4--\f aes2~ aes8 f     | g-. ees~ ees2.
f4-- aes2~ aes8 f       | g-. ees16 f g2.
f4-- des2 ees8-. d16 c~ | c1
des4-- f2 ees8-. d16 c~ | c1

\mark \default %J
f4--\f aes2~ aes8 f     | g-. ees~ ees2.
r4 aes2~-> aes8 f       | g-. ees16 f g2.
f4-- des2 ees8-. d16 c~ | c1
des4-- f2 ees8-. d16 c~ | c1


\mark \default %K
\tuplet 3/2 {g8\<\f g g} \repeat unfold 2 {\tuplet 3/2 {g g g}} g4-.    | 
\repeat unfold 3 {\tuplet 3/2 {g8 g g}} g4-.                            |
\repeat unfold 3 {\tuplet 3/2 {g8 g g}} g4-.                            |
\tuplet 3/2 {g8\!\ff g g} \repeat unfold 2 {\tuplet 3/2 {g8 g g}} g4-. |

\mark \default \time 12/8 \tempo 4. = 132 %L
R1.*8


\mark \default %M
\repeat volta 2 {\TUNnotesM}

\mark \default %N
\repeat percent 3 { r2. r4. aes8 g b | r2. aes8 g b aes g b |}
 r2. r4. aes8 g b |  r2. r4. ees'4. |

 \mark \default %O
 \TUNnotesO

 \mark \default \time 4/4 \tempo 4 = 132 %P
 \relative c'{\TTroiP} 
% fin
 c,4->\ff aes-> g-> des'-> \bar"|."
	 
}
