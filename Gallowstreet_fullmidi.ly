\version "2.18.2"
\include "articulate.ly"
\include "TromUn.ily"
\include "TromDeux.ily"
\include "TromTrois.ily"
\include "TrumpUn.ily"
\include "TrumpDeux.ily"
\include "TrumpTrois.ily"
\include "Bass.ily"
\include "AltSaxUn.ily"
\include "TenorOne.ily"
\include "saxBar.ily"

% ****************************************************************
% Start cut-&-pastable-section
% ****************************************************************

\paper {
  #(set-default-paper-size "a4landscape")
  ragged-bottom = ##t
  % For 3 page layout:
  #(layout-set-staff-size 15)
  bottom-margin =12
  %annotate-spacing = ##t
  %{ %}
  between-system-padding = 0
  ragged-last-bottom = ##f
}

\book 
{
    \header
    {
      title = "Still Feel - Half Alive"
    }
  \score
  {
    \unfoldRepeats
    \articulate
    <<
    \context StaffGroup {
      <<
        \new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Alto Sax"
          \set Staff.midiInstrument = #"trumpet" %"
          \transpose a c {\relative c' {\AltSaxUn} }
        }
        \new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Trumpet"
          \set Staff.midiInstrument = #"trumpet"
            \transpose d c {\relative c'' {\TrumpUn}}
        }
        \new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Trumpet"
          \set Staff.midiInstrument = #"trumpet"
            \transpose d c {\relative c' {\TrumpDeux}}
        }
        \new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Trumpet"
          \set Staff.midiInstrument = #"trumpet"
            \transpose d c {\relative c' {\TrumpTrois}}
        }
        \new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Trombone"
          \set Staff.midiInstrument = #"trombone"
          \relative c' {\TromUn}
        }
        \new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Trombone"
          \set Staff.midiInstrument = #"trombone"
          \relative c' {\TromDeux}
        }
        \new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Trombone"
          \set Staff.midiInstrument = #"trombone"
          \relative c' {\TromTrois}
        }
        % PAS DEUPHO = TROMBONE 3
        \new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Sax Bar"
          \set Staff.midiInstrument = #"tuba" %"
          \transpose a c {\relative c {\BarSax}}
        }
        \new Staff
        {
          #(set-accidental-style 'modern)
          \set Staff.instrumentName = "Tuba"
          \set Staff.midiInstrument = #"tuba" %"
          \transpose d c {\relative c {\Sousa}}
        }
      >>
    }
    >>
    % \layout { }
    \midi {}
  }
}

