\version "2.18.2"
\include "AltSaxUn.ily"


BarSax ={
	\AltSaxIntro

	\mark \default %A
	\repeat percent 3 {a,8-.\f a-. a-. a-. a-. a-. a16( a' a, a') |}
	a,8-. a-. a-. a-. a-. r r4 |

	\repeat percent 3 {a8-.\f a-. a-. a-. a-. a-. a16( a' a, a') |}
	a,8-. a-. a-. a-. a-. r g'16( f e c)  |

	\mark \default %B
	\repeat percent 3 {a8-.\f a-. a-. a-. a-. a-. a16( a' a, a') |}
	a,8-. a-. a-. a-. a-. r g'16( f e c)  |

	\repeat percent 3 {a8-.\f a-. a-. a-. a-. a-. a16( a' a, a') |}
	a,8-. a-. a-. a-. d4-- e-- |

	\mark \default %C
	\tuplet 3/2 {a8\f\< a a a a a a a a} a4 |
	\tuplet 3/2 {e8 e e e e e e e e} e4     |
	\tuplet 3/2 {a,8 a a a a a a a a} a4\!  |
	a'4-> f-> e-> bes'->                   |

	\mark \default %D
	\repeat volta 2 {
		a,8-.\f a-. a-. a-. a-. a-. a16( a' a, a') |
		a,8-. a-. a-. a-. a-. r g'16( f e c)       |
		a8-. a-. a-. a-. a-. a-. a16( a' a, a')   |
		a,8-. a-. a-. a-. d4-- e--                 |
	}

	\mark \default %E
	\repeat volta 2 {
		a,8-. a-. a-. a-. a-. a-. a16( a' a, a') |
		a,8-. a-. a-. a-. a-. r g'16( f e c)       |
		a8-. a-. a-. a-. a-. a-. a16( a' a, a')   |
		a,8-. a-. a-. a-. d4-- e--                 |
	}

	\mark \default %F
	r8 a\mf r a r a r a16( g | a g a8) r a r a r a16( g |
	a g a8) r a r a r a16( g | a g a8) a a16( g a g a8) a a16( g |

	\mark \default %G
	a g a8) r a r a r a16( g | a g a8) r a r a r a16( g |
	a g a8) r a r a r a16( g | a g a8) a a16( g a g a8) a a16( g |

	a g a8) r a r a r a16( g | a g a8) r a r a r a16( g |
	a g a8) r a r a r a-.    | \repeat unfold 8 {a-.}   |

	\mark \default %H
	\repeat percent 6 {a,( a') r a r a r a16 g |}
	a,8( a') r a r a r4 |
	a-> f-> e-> bes'->  |

	\mark \default %I
	\repeat volta 2 {
		a,8-. a-. a-. a-. a-. a-. a16( a' a, a') |
		a,8-. a-. a-. a-. a-. r g'16( f e c)       |
		a8-. a-. a-. a-. a-. a-. a16( a' a, a')   |
		a,8-. a-. a-. a-. d4-- e--                 |
	}

	\mark \default %J
	\repeat volta 2 {
		a,8-. a-. a-. a-. a-. a-. a16( a' a, a') |
		a,8-. a-. a-. a-. a-. r g'16( f e c)       |
		a8-. a-. a-. a-. a-. a-. a16( a' a, a')   |
		a,8-. a-. a-. a-. d4-- e--                 |
	}
	\mark \default %K
	\tuplet 3/2 {a8 a a a a a a a a} a4-.       |
	\tuplet 3/2 {e8\< e e e e e e e e} e4-.     |
	\tuplet 3/2 {a,8 a a a a a a a a} a4-.\!\ff |
	\tuplet 3/2 {a'8 a a a a a a a a} a4-.      |

	\mark \default  \time 12/8 \tempo 4. = 132 %L
	\repeat percent 8 {a,8 \repeat unfold 8 {a} a4 r8 |}

	\mark \default  %M
	a8^\markup{Trombone} \repeat unfold 8 {a} a4 r8 |\repeat percent 7 {a8 \repeat unfold 8 {a} a4 r8 |}

	\mark \default  %N
	a8^\markup{Trompette} \repeat unfold 8 {a} a4 r8 |\repeat percent 7 {a8
		\repeat unfold 8 {a} a4 r8 |}

	\mark \default %O
	\repeat percent 3 {a4. a a c4 e8 | a,4. a a f'4 e8 |} |
	a,4. a a e'4 g8 | a4. a a r |

	\mark \default  \time 4/4 \tempo 4 = 132 %P
	R1*4 | a1( | g | f | e)\fermata |

	a4-> f-> e-> bes'->           \bar"|."


}