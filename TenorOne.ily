\version "2.18.2"
\include "AltSaxUn.ily"

TenorSaxUn = {
	\transpose a d' {\relative c'' {\AltSaxIntro}}

	\mark \default %A
	R1*8\!

	\mark \default %B
	\repeat volta 2 {
		\repeat percent 2 {r8 d r d r d r f16 g |}
			d8-. d r d r d r f16 g |
			d8-. d r d g4-- a-- |
	}

	\mark \default %C
	\tuplet 3/2 {d,8\f\< d d d d d d d d} d4   |
	\tuplet 3/2 {a'8 a a a a a a a a} a4   |
	\tuplet 3/2 {d,8 d d d d d d d d} d4\! |
	a'4->\ff g-> ees-> bes'->

	\mark \default %D
	ees,4-- ees'2~ ees8 c | d-. a~ a2. |
	ees4-- ees'2~ ees8 c  | d-.bes16 g a2. |
	bes4-- g2 a8-. f16 d~ | d1 |
	ees4-. g2 a8-. f16 d~  | d1 |

	\mark \default %E
	ees4-- ees'2~ ees8 c | d-. a~ a2. |
	r4 ees'2~ ees8 c      | d-.bes16 g a2. |
	bes4-- g2 a8-. f16 d~ | d1 |
	ees4-. g2 a8-. f16 d~  | d1|

	\mark \default %F
	R1*4
	\mark \default %G
	R1*8
	\mark \default %H
	R1*7
	a'4->\ff g-> ees-> bes'->

	\mark \default %I
	ees,4--\f ees'2~ ees8 c | d-. a~ a2. |
	ees4-- ees'2~ ees8 c    | d-.bes16 g a2. |
	bes4-- g2 a8-. f16 d~   | d1 |
	ees4-. g2 a8-. f16 d~    | d1 |

	\mark \default %J
	ees4-- ees'2~ ees8 c | d-. a~ a2. |
	r4 ees'2~ ees8 c      | d-.bes16 g a2. |
	bes4-- g2 a8-. f16 d~ | d1 |
	ees4-. g2 a8-. f16 d~  | d1|

	\mark \default %K
	\tuplet 3/2 {d8\f d d d d d d d d} d4-.     |
	\tuplet 3/2 {a'8\< a a a a a a a a} a4-.    |
	\tuplet 3/2 {d,8 d d d d d d d d} d4-.\!\ff |
	\tuplet 3/2 {a'8 a a a a a a a a} a4-.    |

	\mark \default  \time 12/8 \tempo 4. = 132 %L
	\repeat percent 8 {d,8 \repeat unfold 8 {d} ees4 r8 |}

	\mark \default  %M
	\repeat percent 8 {d8 \repeat unfold 8 {d} ees4 r8 |}

	\mark \default  %N
	\repeat percent 8 {d8 \repeat unfold 8 {d} ees4 r8 |}

	\mark \default  %O
	\repeat volta 2 {
		\repeat unfold 3 { d4. d d r4 cis8}
	}
	\alternative {
		{d4. d d a'8 a f}
		{d4. d d r}
	}

	\mark \default  \time 4/4 \tempo 4 = 132 %P
	a'1( | g | f | e)
	a( | g | f | ees)

	a4->\ff g-> ees-> bes'->
	\bar"|."
}