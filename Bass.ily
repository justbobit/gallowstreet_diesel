\version "2.20.0"

Sousa =
{
	R1*10^\markup{Intro} |

	\mark \default %A
	R1*8
	\mark \default %B
	\repeat percent 4 {c4\f r8 c r4 r8 ees16 f | c4 r8 c r2 |}

	\mark \default %C
	R1*3 | c'4->\ff aes-> g-> des'-> |

	\mark \default %D
	\repeat percent 8 {c,4\f r8 c r4 r8 ees16 f | }

	\mark \default %E
	\repeat percent 8 {c4 r8 c r4 r8 ees16 f | }

	\mark \default %F
	c4 r8 c r2 | R1*3

	\mark \default %G
	\repeat volta 2 {c4 r8 c r2 | R1*3}

	\mark \default %H
	\repeat percent 7 {c4 r8 c r2 |}
	c'4->\ff aes-> g-> des'-> |

	\mark \default %I
	\repeat percent 8 {c,4\f r8 c r4 r8 ees16 f | }

	\mark \default %J
	\repeat percent 7 {c4 r8 c r4 r8 ees16 f | }
	c4 r8 c r2 |

	\mark \default %K
	R1*4

	\mark \default  \time 12/8 \tempo 4. = 132 %L
	c8\f c c c c c c c c c4 r8
	\repeat percent 7 {c8 c c c c c c c c c4 r8}

	\mark \default  %M
	c8^\markup{Trombone} c c c c c c c c c4 r8
	\repeat percent 7 {c8 c c c c c c c c c4 r8}

	\mark \default  %N
	c8^\markup{Trompette} c c c c c c c c c4 r8
	\repeat percent 7 {c8 c c c c c c c c c4 r8}

	\mark \default %O
	\repeat percent 3 {c4. c c ees4 f8 | c4. c c aes'4 g8 |  }
	c,4. c c g'4 bes8 | c4. c c r4. |

	\mark \default  \time 4/4 \tempo 4 = 132 %P
	c1 bes aes g c, bes aes g\fermata

	c'4->\ff aes-> g-> des'-> \bar"|."




}