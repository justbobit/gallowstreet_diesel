\version "2.18.2"
\include "TrumpUn.ily"


TrumpDeux = {
	f1(\p^\markup{Intro}\tempo 4=100 | f2 e2 | f1) | 
	e2( g | a2.) g4( | f1 | g1) | f2 e4 f | g1 | a\<\fermata |
	\mark \default %A
	R1*8\!
	\mark \default %B
	R1*8
	\mark \default %C
	\tuplet 3/2 {r8\f\< d d \repeat unfold 2 {r d d} } d4|
	\tuplet 3/2 {r8 d d \repeat unfold 2 {r d d} } d4|
	\tuplet 3/2 {r8 d d \repeat unfold 2 {r d d} } d4\!|
	d->\ff bes-> a-> ees'-> 

	\mark \default %D
	ees,--\f ees'2~ ees8 c | d-. a~ a2. | ees4-- ees'2~ ees8 c | d-. bes16 g a2.
	bes4-- g2 a8-. f16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default  %E
	ees4-- ees'2~ ees8 c | d-. a~ a2. | r4 ees'2~ ees8 c | d-. bes16 g a2.
	bes4-- g2 a8-. f16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default  %F
	R1*4
	\mark \default  %G
	R1*8
	\mark \default  %H
	R1*2 | a'16\>\f a r16 a a r a a r a a r a a r a | a r a a\!\p r4 r2 |
	R1*2 | a16\>\f a r16 a a r a a r a a r a a r a\!\p | d4->\ff bes-> a-> ees'-> 

	\mark \default %I
	ees,4--\f ees'2~ ees8 c | d-. a~ a2. | ees4-- ees'2~ ees8 c | d-. bes16 g a2.
	bes4-- g2 a8-. f16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default  %J
	ees4-- ees'2~ ees8 c | d-. a~ a2. | r4 ees'2~ ees8 c | d-. bes16 g a2.
	bes4-- g2 a8-. f16 d~ | d1 | ees4-- g2 a8-. f16 d~ | d1 |

	\mark \default  %k
	\tuplet 3/2 { \repeat unfold 9 {a'8}} a4-. |
	\tuplet 3/2 { a8\< \repeat unfold 8 {a8}} a4-. |
	\tuplet 3/2 { d8 \repeat unfold 8 {d8}} d4-.\! |
	\tuplet 3/2 { d8\ff \repeat unfold 8 {d8}} d4-. |

	\mark \default \time 12/8 \tempo 4. = 132 %L
	R1.*8
	\mark \default %M
	R1.*8	

	\mark \default %N
	\repeat percent 8 {d8-.\f c-. a c-. a-. g-. a-. g-. f-. g-. r r | }

	\mark \default %O
	\relative c'' {\TrumpUnO}

	\mark \default \time 4/4 \tempo 4 = 132 %P
	\transpose c d { \relative c'' {\TUNnotesP}}
	
	d'4->\ff bes-> a-> ees'-> 
	\bar"|."

	% d'1( | c | bes | a)
	% d(  | e | d   | ees)\fermata \bar"|."
}