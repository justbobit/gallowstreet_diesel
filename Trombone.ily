TromOne =
{
%Intro
r4^\markup{Intro}\tempo 4=100  <aes c ees>\pp <g b d> r |
\repeat unfold 6 {r4 <aes c ees>\pp <g b d> r |}
r <aes c ees> <g b d>  <ees aes c> | 
<f bes d>1\<              | <g b d>\!\mp\fermata
\mark "A" \tempo 4 = 132
R1*8
r8 <c, g'>-.\f \repeat unfold 2 {r8 <c g'>-. } r <ees g>16\glissando <f aes>16 |
\repeat unfold 3 {r8 <c g'>-. } r <ees g>16\glissando <f aes>16 |
\repeat unfold 3 {<c g'>8 <c g'>-.} r <ees g>16\glissando <f aes>16 |

\repeat unfold 2 {r8 <c g'>-. } < f bes>4-> <g c>-> |

\repeat unfold 2 {
		
	\repeat unfold 3 {r8 <c, g'>-. } r <ees g>16\glissando <f aes>16 |
	\repeat unfold 3 {r8 <c g'>-. } r <ees g>16\glissando <f aes>16 |
	\repeat unfold 3 {<c g'>8 <c g'>-.} r <ees g>16\glissando <f aes>16 |

	\repeat unfold 2 {r8 <c g'>-. } < f bes>4-> <g c>-> |
	}

}