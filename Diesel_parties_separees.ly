\version "2.20.0"
\include "Bass.ily"
\include "TromUn.ily"
\include "TromDeux.ily"
\include "TromTrois.ily"
\include "Trombone.ily"
\include "TrumpUn.ily"
\include "TrumpDeux.ily"
\include "TrumpTrois.ily"
\include "AltSaxUn.ily"
\include "TenorOne.ily"
\include "saxBar.ily"
\include "clar.ily"


% ****************************************************************
% Start cut-&-pastable-section
% ****************************************************************

#(set! paper-alist (cons '("my size" . (cons (* 29.7 cm) (* 21 cm))) paper-alist))


\paper {
  #(set-paper-size "my size")
  ragged-bottom = ##t
  % For 3 page layout:
  #(layout-set-staff-size 19)
  top-margin    =2
  bottom-margin =2
  left-margin   =8
  right-margin  =8
  
  %annotate-spacing = ##t
  %{ %}
  between-system-padding = 0
  ragged-bottom=##f
  ragged-last-bottom=##f 
  
}

\book
{
  \header
  {
    title = "Diesel - Gallowstreet"
    subtitle = ##f
    composer = "Gallowstreet - Arrgt. Bobby V3 - 04/22"
    tagline = ##f
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Clarinet"
        \set Staff.midiInstrument = #"trombone"
        \transpose a d' {\relative c''' {\clef "treble" \key c \major \ClarUn}
              }}
      \layout {
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \set Score.markFormatter = #format-mark-box-alphabet
        #(layout-set-staff-size 21)
      }

    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Alt Sax"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c''' {\clef "treble" \key c \major \AltSaxUn}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
        #(layout-set-staff-size 21)
      }

    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Tenor Sax"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c' {\clef "treble" \key f \major \TenorSaxUn}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
        #(layout-set-staff-size 24)
      }

    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Baritone Sax"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c'' {\clef "treble" \key c \major \BarSax}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
        #(layout-set-staff-size 22)
      }

    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Tenor 2"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \transpose  a d' {\relative c'' {\clef "treble" \key c \major \BarSax}}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
        #(layout-set-staff-size 22)
      }

    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Trompette 1"
        \set Staff.midiInstrument = #"trumpet"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c'' {\clef "treble" \key f \major
        \TrumpUn}
      }
      \layout {
        #(layout-set-staff-size 21)
        \set Score.markFormatter = #format-mark-box-alphabet

      }
    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Trompette 2"
        \set Staff.midiInstrument = #"trumpet"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c' {\clef "treble" \key f \major
        \TrumpDeux}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
        #(layout-set-staff-size 22)
      }
    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Trompette 3"
        \set Staff.midiInstrument = #"trumpet"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c' {\clef "treble" \key f \major
        \TrumpTrois}
      }
      \layout {
        #(layout-set-staff-size 22)
        \set Score.markFormatter = #format-mark-box-alphabet
      }
    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Trombone 1"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c' {\clef "bass" \key c \minor \large
        \TromUn}
      }
      \layout {
        #(layout-set-staff-size 20)
        \set Score.markFormatter = #format-mark-box-alphabet
      }
    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Trombone 2"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c' {\clef "bass" \key c \minor \large
        \TromDeux}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
      }
    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Trombone 3"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c' {\clef "bass" \key c \minor \large \TromTrois}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
        }
    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Eupho"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \transpose c d {\relative c'' {\clef "treble" \key c \minor \large
        \TromTrois}}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
        }
    }
  }
  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Sousa"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \transpose c d {\relative c' {\clef "treble" \key c \minor \large
        \Sousa}}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
        #(layout-set-staff-size 22)
        }
    }
  }

  \bookpart{
    \score{
      \new Staff
      {
        #(set-accidental-style 'modern)
        \set Staff.instrumentName = "Sousa ut"
        \set Staff.midiInstrument = #"trombone"
        \compressFullBarRests \override MultiMeasureRest.expand-limit = #0
        \relative c, {\clef "bass" \key c \minor \large
        \Sousa}
      }
      \layout {
        \set Score.markFormatter = #format-mark-box-alphabet
        #(layout-set-staff-size 22)
        }
    }
  }
}